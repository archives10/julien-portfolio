class CreateSeries < ActiveRecord::Migration[5.1]
  def change
    create_table :series do |t|
      t.string :name
      t.references :picture, foreign_key: true

      t.timestamps
    end
  end
end
