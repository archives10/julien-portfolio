class AddDescriptionToPictures < ActiveRecord::Migration[5.1]
  def change
    add_column :pictures, :description, :string
    add_column :pictures, :format, :string
    add_column :pictures, :author, :string
  end
end
