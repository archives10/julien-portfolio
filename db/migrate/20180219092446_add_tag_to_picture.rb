class AddTagToPicture < ActiveRecord::Migration[5.1]
  def change
    add_column :pictures, :tag, :string
  end
end
