class Picture < ApplicationRecord
  belongs_to :user
  mount_uploader :photo, PhotoUploader

  validates :name, uniqueness: true, presence: true
end
