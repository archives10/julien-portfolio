class PagesController < ApplicationController
  skip_before_action :authenticate_user!, only: :home, raise: false
  def home
  end

  def about
  end

  def contact
  end

  def profile
    @pictures = Picture.all
  end
end
