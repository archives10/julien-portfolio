class PicturesController < ApplicationController
    before_action :set_picture, only: [:show, :edit, :update, :destroy]
    protect_from_forgery with: :null_session

    def index
        @serie_01 = Picture.where("tag = '1'")
        @serie_02 = Picture.where("tag = '2'")
        @serie_03 = Picture.where("tag = '3'")
        @serie_04 = Picture.where("tag = '4'")
        @serie_05 = Picture.where("tag = '5'")
    end

    def show
        serie = Picture.where(tag: @picture.tag)
        picture = serie.index(@picture)
        index_previous = picture - 1
        index_next = picture + 1

        if index_previous >= 0
            @previous = serie[index_previous]
        end
        if serie[index_next]
            @next = serie[index_next]
        end
    end

    def new
        @picture = Picture.new
    end

    def create
        @picture = Picture.new(picture_params)
        @picture.user = current_user
        if @picture.save
            redirect_to profile_path
        else
            render :new
        end
    end

    def edit
    end

    def update
        @picture.update(picture_params)
        if @picture.save
            redirect_to picture_path(@picture)
        else
            render :edit
        end
    end

    def destroy
        @picture.destroy
        redirect_to profile_path
    end

private

    def set_picture
        @picture = Picture.find(params[:id])
    end

    def picture_params
        params.require(:picture).permit(:name, :description, :format, :tag, :author, :photo)
    end
end
