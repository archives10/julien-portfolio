Rails.application.routes.draw do
  devise_for :users
  
  root to: "pages#home"

  as :user do
    get 'julien', to: 'devise/sessions#new'
  end

  get '/about', to: 'pages#about'
  get '/contact', to: 'pages#contact'
  get '/profile', to: 'pages#profile'

  resources :pictures


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
